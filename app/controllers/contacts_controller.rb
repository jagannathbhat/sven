# frozen_string_literal: true

class ContactsController < ApplicationController
  before_action :authenticate_user_using_x_auth_token
  before_action :load_contact, except: [:create, :index]

  def index
    contacts = Contact.where(user_id: @current_user.id)
    render status: :ok, json: { contacts: contacts }
  end

  def create
    contact = Contact.new(contact_params.merge(user_id: @current_user.id))
    if contact.save
      render status: :ok, json: { notice: t("create_success", entity: "Contact") }
    else
      errors = contact.errors.full_messages.to_sentence
      render status: :unprocessable_entity, json: { errors: errors }
    end
  end

  def destroy
    if @contact.destroy
      render status: :ok, json: { notice: t("delete_success", entity: "Contact") }
    else
      render status: :unprocessable_entity, json: { errors: @contact.errors.full_messages.to_sentence }
    end
  end

  def show
    if @contact
      render status: :ok, json: { contact: @contact }
    else
      render status: :unprocessable_entity, json: { errors: @contact.errors.full_messages.to_sentence }
    end
  end

  def update
    if @contact.update(contact_params)
      render status: :ok, json: { notice: t("update_success", entity: "Contact") }
    else
      render status: :unprocessable_entity, json: { errors: @contact.errors.full_messages.to_sentence }
    end
  end

  private

    def contact_params
      params.require(:contact).permit(:name, :email, :phone_number)
    end

    def load_contact
      @contact = Contact.find_by_slug!(params[:slug])
    rescue ActiveRecord::RecordNotFound => errors
      render json: { errors: errors }
    end
end
