# frozen_string_literal: true

class Contact < ApplicationRecord
  validates :slug, uniqueness: true
  belongs_to :user

  before_create :set_slug

  private

    def set_slug
      itr = 1
      loop do
        name_slug = name.parameterize
        slug_candidate = itr > 1 ? "#{name_slug}-#{itr}" : name_slug
        break self.slug = slug_candidate unless Contact.exists?(slug: slug_candidate)

        itr += 1
      end
    end
end
