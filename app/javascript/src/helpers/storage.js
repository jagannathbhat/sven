const setToLocalStorage = ({ authToken, authEmail, userId }) => {
  localStorage.setItem("authToken", authToken);
  localStorage.setItem("authEmail", authEmail);
  localStorage.setItem("authUserId", userId);
};

const getFromLocalStorage = key => {
  return localStorage.getItem(key);
};

export { setToLocalStorage, getFromLocalStorage };
