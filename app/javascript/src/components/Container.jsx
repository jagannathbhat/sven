import React from "react";

import PropTypes from "prop-types";

const Container = ({ children }) => {
  return (
    <>
      <div className="mx-auto max-w-7xl p-4 sm:px-6 lg:px-8">
        <div className="max-w-3xl mx-auto">{children}</div>
      </div>
    </>
  );
};

Container.propTypes = {
  children: PropTypes.node.isRequired
};

export default Container;
