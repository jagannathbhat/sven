import React, { useState } from "react";

import usersApi from "apis/users";
import LoginForm from "components/Authentication/Form/LoginForm";
import { setAuthHeaders } from "apis/axios";
import { setToLocalStorage } from "helpers/storage";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const response = await usersApi.login({ login: { email, password } });
      logger.log({
        authToken: response.data.authentication_token,
        userEmail: email,
        userId: response.data.id
      });
      setToLocalStorage({
        authToken: response.data.authentication_token,
        authEmail: email,
        userId: response.data.id
      });
      setAuthHeaders();
      setLoading(false);
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  };

  return (
    <LoginForm
      setEmail={setEmail}
      setPassword={setPassword}
      loading={loading}
      handleSubmit={handleSubmit}
    />
  );
};

export default Login;
