import React from "react";
import PropTypes from "prop-types";

const Button = ({ type = "button", buttonText, onClick, loading }) => {
  return (
    <div className="mt-6">
      <button
        type={type}
        onClick={onClick}
        className="bg-bb-purple border border-transparent duration-150 ease-in-out flex font-medium group
        justify-center mx-auto px-4 py-2 relative rounded-md text-sm leading-5 text-white transition
        hover:bg-opacity-90 focus:outline-none"
      >
        {loading ? "Loading..." : buttonText}
      </button>
    </div>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  buttonText: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func
};
export default Button;
