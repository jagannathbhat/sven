import React from "react";
import NavItem from "./NavItem";

import usersApi from "apis/users";
import { getFromLocalStorage, setToLocalStorage } from "helpers/storage";
import { resetAuthTokens } from "apis/axios";

const NavBar = () => {
  const authEmail = getFromLocalStorage("authEmail");

  const handleLogout = async () => {
    try {
      await usersApi.logout();
      setToLocalStorage({
        authToken: null,
        userEmail: null,
        userId: null
      });
      resetAuthTokens();
      window.location.href = "/";
    } catch (error) {
      logger.error(error);
    }
  };

  return (
    <nav className="bg-white shadow">
      <div className="px-2 mx-auto max-w-7xl sm:px-4 lg:px-8">
        <div className="flex justify-between h-16">
          <div className="flex px-2 lg:px-0">
            <div className="hidden lg:flex">
              <NavItem name="sven" path="/" />
            </div>
          </div>
          <div className="flex items-center justify-end">
            <span
              className="font-semibold leading-5 pr-4 pt-1 px-1 text-sm text-bb-gray-600
              text-opacity-50 hover:text-bb-gray-600"
            >
              {authEmail}
            </span>
            <a
              className="border-b-2 border-transparent cursor-pointer ease-in-out
              font-semibold leading-5 pt-1 px-1 text-sm text-bb-gray-600
              text-opacity-50 transition duration-150
              focus:outline-none focus:text-bb-gray-700
             hover:text-bb-gray-600"
              onClick={handleLogout}
            >
              LogOut
            </a>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
