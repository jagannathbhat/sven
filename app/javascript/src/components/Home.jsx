import React, { useState, useEffect } from "react";
import { isNil, isEmpty, either } from "ramda";
import { Link } from "react-router-dom";

import Button from "components/Button";
import Container from "components/Container";
import FAB from "./FAB";
import Table from "components/Contacts/Table";
import PageLoader from "components/PageLoader";
import contactsApi from "apis/contacts";

const Home = ({ appLoading }) => {
  const [contacts, setContacts] = useState([]);
  const [loading, setLoading] = useState(true);

  const deleteContact = async slug => {
    try {
      await contactsApi.destroy(slug);
      await fetchContacts();
      setLoading(false);
    } catch (error) {
      setLoading(false);
      logger.error(error);
    }
  };

  const fetchContacts = async () => {
    try {
      const response = await contactsApi.list();
      setContacts(response.data.contacts);
      setLoading(false);
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (!appLoading) fetchContacts();
  }, []);

  if (loading) {
    return (
      <div className="w-screen h-screen">
        <PageLoader />
      </div>
    );
  }

  if (either(isNil, isEmpty)(contacts)) {
    return (
      <Container>
        <h1 className="text-xl leading-5 text-center">
          Please tell me about your friends
          <Link to="/contact/new">
            <Button buttonText="Add Contact" />
          </Link>
        </h1>
      </Container>
    );
  }

  return (
    <Container>
      <Table data={contacts} deleteContact={deleteContact} />
      <Link to="/contacts/create">
        <FAB></FAB>
      </Link>
    </Container>
  );
};

export default Home;
