import React from "react";
import PropTypes from "prop-types";

const FAB = ({ type = "button", onClick, iconClassName = "ri-add-line" }) => {
  return (
    <button
      type={type}
      onClick={onClick}
      className="bg-bb-purple border border-transparent bottom-0 duration-150 ease-in-out fixed flex font-medium group
        h-16 items-center justify-center leading-5 m-4 right-0 rounded-full text-5xl text-white transition w-16 z-50
        hover:bg-opacity-90 focus:outline-none"
      // style={{ bottom: 12, right: 12 }}
    >
      <i className={iconClassName}></i>
    </button>
  );
};

FAB.propTypes = {
  type: PropTypes.string,
  onClick: PropTypes.func,
  iconClassName: PropTypes.string
};

export default FAB;
