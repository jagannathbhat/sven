import React, { useState } from "react";

import ContactForm from "./Form/ContactForm";
import Container from "components/Container";
import contactsApi from "apis/contacts";

const CreateContact = ({ history }) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      await contactsApi.create({
        contact: { email, name, phone_number: phoneNumber }
      });
      setLoading(false);
      history.push("/");
    } catch (error) {
      logger.error(error);
      setLoading(false);
    }
  };

  return (
    <Container>
      <ContactForm
        handleSubmit={handleSubmit}
        loading={loading}
        setEmail={setEmail}
        setName={setName}
        setPhoneNumber={setPhoneNumber}
      />
    </Container>
  );
};

export default CreateContact;
