import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import ContactForm from "./Form/ContactForm";
import Container from "components/Container";
import PageLoader from "components/PageLoader";
import contactsApi from "apis/contacts";

const EditContact = ({ history }) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(true);
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const { slug } = useParams();

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      await contactsApi.update({
        slug,
        payload: { contact: { email, name, phone_number: phoneNumber } }
      });
      setLoading(false);
      history.push("/");
    } catch (error) {
      setLoading(false);
      logger.error(error);
    }
  };

  const fetchContactDetails = async () => {
    try {
      const response = await contactsApi.show(slug);
      setName(response.data.contact.name);
      setEmail(response.data.contact.email);
      setPhoneNumber(response.data.contact.phone_number);
    } catch (error) {
      logger.error(error);
    } finally {
      setPageLoading(false);
    }
  };

  useEffect(() => {
    fetchContactDetails();
  }, []);

  if (pageLoading) {
    return (
      <div className="w-screen h-screen">
        <PageLoader />
      </div>
    );
  }

  return (
    <Container>
      <ContactForm
        email={email}
        handleSubmit={handleSubmit}
        loading={loading}
        name={name}
        phoneNumber={phoneNumber}
        setEmail={setEmail}
        setName={setName}
        setPhoneNumber={setPhoneNumber}
        type="update"
      />
    </Container>
  );
};

export default EditContact;
