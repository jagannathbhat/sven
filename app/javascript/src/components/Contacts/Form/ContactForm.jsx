import React from "react";

import Button from "components/Button";
import Input from "components/Input";

const ContactForm = ({
  email,
  handleSubmit,
  loading,
  name,
  phoneNumber,
  setEmail,
  setName,
  setPhoneNumber,
  type = "create"
}) => {
  return (
    <form className="max-w-lg mx-auto" onSubmit={handleSubmit}>
      <Input
        label="Full Name"
        value={name}
        onChange={e => setName(e.target.value)}
      />
      <Input
        label="Email Address"
        value={email}
        onChange={e => setEmail(e.target.value)}
      />
      <Input
        label="Phone Number"
        value={phoneNumber}
        onChange={e => setPhoneNumber(e.target.value)}
      />
      <Button
        type="submit"
        buttonText={type === "create" ? "Create Contact" : "Update Contact"}
        loading={loading}
      />
    </form>
  );
};

export default ContactForm;
