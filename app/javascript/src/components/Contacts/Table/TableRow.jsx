import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { colors } from "tailwindcss/defaultTheme";

const TableRow = ({ data, deleteContact }) => {
  return (
    <tbody className="bg-white divide-y divide-gray-200">
      {data.map(rowData => (
        <tr key={rowData.id}>
          <td></td>
          <td
            className="block w-64 px-6 py-4 text-sm font-medium
            leading-8 text-bb-purple capitalize truncate"
          >
            {rowData.name}
          </td>
          <td
            className="px-6 py-4 text-sm font-medium
            leading-5 text-bb-gray whitespace-no-wrap"
          >
            <a href={`mailto:${rowData.email}`}>{rowData.email}</a>
          </td>
          <td
            className="px-6 py-4 text-sm font-medium
            leading-5 text-bb-gray whitespace-no-wrap"
          >
            <a href={`tel:${rowData.phone_number}`}>{rowData.phone_number}</a>
          </td>
          <td>
            <Link to={`contact/${rowData.slug}/edit`}>
              <i className="ri-pencil-line"></i>
            </Link>
          </td>
          <td>
            <a
              style={{ color: colors.red[500], cursor: "pointers" }}
              onClick={() => deleteContact(rowData.slug)}
            >
              <i className="ri-delete-bin-line"></i>
            </a>
          </td>
        </tr>
      ))}
    </tbody>
  );
};

TableRow.propTypes = {
  data: PropTypes.array.isRequired,
  deleteContact: PropTypes.func.isRequired
};

export default TableRow;
