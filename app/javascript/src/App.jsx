import React, { useEffect, useState } from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { either, isEmpty, isNil } from "ramda";

import CreateContact from "components/Contacts/CreateContact";
import EditContact from "components/Contacts/EditContact";
import Home from "components/Home";
import Login from "components/Authentication/Login";
import NavBar from "./components/NavBar";
import PrivateRoute from "components/Common/PrivateRoute";
import Signup from "components/Authentication/Signup";
import { setAuthHeaders } from "apis/axios";
import { initializeLogger } from "common/logger";
import { getFromLocalStorage } from "helpers/storage";

const App = () => {
  const [loading, setLoading] = useState(true);
  const authToken = getFromLocalStorage("authToken");
  const isLoggedIn = !either(isNil, isEmpty)(authToken) && authToken !== "null";

  useEffect(() => {
    /*eslint no-undef: "off"*/
    initializeLogger();
    setAuthHeaders(setLoading);
  }, []);

  return (
    <Router>
      {isLoggedIn && <NavBar />}
      <Switch>
        <PrivateRoute
          component={() => <Home appLoading={loading} />}
          condition={isLoggedIn}
          exact
          path="/"
        />
        <PrivateRoute
          component={EditContact}
          condition={isLoggedIn}
          exact
          path="/contact/:slug/edit"
        />
        <PrivateRoute
          component={CreateContact}
          condition={isLoggedIn}
          exact
          path="/contact/new"
        />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </Router>
  );
};

export default App;
