import axios from "axios";

const create = payload => axios.post("/contacts/", payload);

const destroy = slug => axios.delete(`/contacts/${slug}`);

const list = () => axios.get("/contacts");

const show = slug => axios.get(`/contacts/${slug}`);

const update = ({ slug, payload }) => axios.put(`/contacts/${slug}`, payload);

const contactsApi = {
  create,
  destroy,
  list,
  show,
  update
};

export default contactsApi;
