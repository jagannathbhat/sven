# frozen_string_literal: true

class AddSlugToContact < ActiveRecord::Migration[6.1]
  def change
    add_column :contacts, :slug, :string, unique: true, null: false
  end
end
