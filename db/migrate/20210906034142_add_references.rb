# frozen_string_literal: true

class AddReferences < ActiveRecord::Migration[6.1]
  def change
    add_reference :contacts, :user, foreign_key: { on_delete: :cascade }, index: false
  end
end
