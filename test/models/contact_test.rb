# frozen_string_literal: true

require "test_helper"

class ContactTest < ActiveSupport::TestCase
  def setup
    @user = build(:user)
    @contact = build(:contact, user: @user)
  end

  def test_contacts_should_have_unique_slug
    @contact.save!
    second_contact = create(:contact, user: @user)

    assert_not_same @contact.slug, second_contact.slug
  end

  def test_task_slug_is_parameterized_name
    name = @contact.name
    @contact.save!
    assert_equal name.parameterize, @contact.slug
  end

  def test_task_slug_is_different_for_same_name
    second_contact = build(:contact, user: @user)
    second_contact.name = @contact.name
    @contact.save!
    second_contact.save!

    assert_equal @contact.slug, @contact.name.parameterize
    assert_equal second_contact.slug, second_contact.name.parameterize + "-2"
  end

  def test_updating_name_does_not_update_slug
    @contact.save!

    slug_before_update = @contact.slug
    contact_updated_name = "Updated Name"
    @contact.update(name: contact_updated_name)

    assert_equal @contact.name, contact_updated_name
    assert_equal slug_before_update, @contact.slug
  end

  def test_contacts_should_have_user
    @contact.user = nil

    assert_not @contact.save
  end
end
