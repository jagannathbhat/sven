# frozen_string_literal: true

require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = build(:user)
  end

  def test_users_should_have_unique_auth_token
    @user.save!
    second_user = create(:user)

    assert_not_same @user.authentication_token, second_user.authentication_token
  end

  def test_user_shouldnt_have_blank_email
    @user.email = ""

    assert_not @user.save
  end

  def test_user_should_have_valid_email
    @user.email = "invalid@email"

    assert_not @user.save
  end

  def test_user_shouldnt_have_short_password
    @user.password = "pass"

    assert_not @user.save
  end

  def test_user_password_and_password_confirmation_should_match
    @user.password = "welcomes"

    assert_not @user.save
  end

  def test_user_should_convert_email_to_lowercase
    email_input = Faker::Internet.email.upcase
    @user.email = email_input
    @user.save!

    assert_equal @user.email, email_input.downcase
  end
end
