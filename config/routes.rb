# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :contacts, only: %i[index create destroy show update], param: :slug
  resource :sessions, only: %i[create destroy]
  resources :users, only: :create

  root "home#index"
  get "*path", to: "home#index", via: :all
end
